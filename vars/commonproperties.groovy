#!/bin/bash/env groovy
def readProperty(Map stepParams)
{
    config = readYaml file: "${stepParams.file}" 
    return config
}
