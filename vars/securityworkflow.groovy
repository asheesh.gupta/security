def call(Map stepParams)
{
    scmCheckout()
    readPropertyFile(filepath: "${stepParams.file}")  
    securityToolFunction() 
}
def scmCheckout(){
    try{
        vcsUtils.checkoutCode()
    }
   catch(e){
        notification.errorInfo(
        message: "Error in reading property file",
        slack_channel: "$config.SLACK"
        color: "danger"
        environment: "${config.ENVIRONMENT}"
        error: e
        )
    }    
}
def readPropertyFile(Map stepParams){
    try{
        config = commonUtils.readProperty(file: "${stepParams.filepath}")
        notification.sendSlackNotification(
        slackChannel:   "${config.SLACK}",
        buildStatus:    "good",
        environment:    "${config.ENVIRONMENT}",
        message:        "Successfull in Reading Config File"
       )
    }
   catch(e){
        notification.errorInfo(
        message: "Error in reading property file",
        slack_channel: "$config.SLACK"
        color: "danger"
        environment: "${config.ENVIRONMENT}"
        error: e
        )
    }    
}
def securityToolFunction(){
    securitylibrary.securityTool(config)
}
