#!/bin/bash/env groovy

def slackNotification(Map info)
{ 
    slackSend (channel: "$info.chn", 
    message: "$info.msg : Job_NAME '${env.JOB_NAME}' \n  BUILD_URL '${env.BUILD_URL}' ") 
}

def errorInfo(Map data){
    slackNotification(
        msg: "$data.message", 
        chn: "$data.channel",
        col: 'danger'
        )
}

def successInfo(Map data){
    slackNotification( 
        msg : "$data.message", 
        chn: "$data.channel",
        col: 'good'
        )
}