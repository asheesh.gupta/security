#!/bin/bash/env groovy

def securityTool(config){

  try{
    if("${config.ZAP_SCAN}" == "true"){
        zap(config)
      }
    if("${config.NIKTO_SCAN}" == "true"){
        nikto(config)
      }
  }
    
  catch(e){
      notification.errorInfo(
      message: "DID NOT SPECIFY ANY OF THE SCANNING TOOL TRUE",
      slack_channel: "$config.SLACK"
      color: "danger"
      environment: "${config.ENVIRONMENT}"
      error: e
      )
        
  }

}
def zap(config){
    
  try{      
    config.NAME.each {
      println " ${it}"
      def data = sh(script:""" echo ${it} | sed -e 's~https://www.~report~g'""",returnStdout: true).trim()     
      sh """ cd /tmp; docker run -v /tmp:/zap/wrk/:rw -t owasp/zap2docker-stable zap-baseline.py -t ${it} -g gen.conf -r "${data}.html" || true """    
      sh"""cp /tmp/"${data}.html" ."""
    }
      html_report()
      notification.sendSlackNotification(
      slackChannel:   "${config.SLACK}",
      buildStatus:    "good",
      environment:    "${config.ENVIRONMENT}",
      message:        "Successfull in Performing ZAP Testing and Publishing report"
      )
  }
  catch(e){
      notification.errorInfo(
      message: "Error in TESTING with ZAP ",
      slack_channel: "$config.SLACK"
      color: "danger"
      environment: "${config.ENVIRONMENT}"
      error: e
      )
    }    
  }

def nikto(config){
  try{
    config.NAME.each {        
      println " ${it}" 
      def data = sh(script:""" echo ${it} | sed -e 's~https://www.~report~g'""",returnStdout: true).trim()  
      sh """ cd /tmp; sudo docker run --rm -v \$(pwd):/tmp -t sullo/nikto -h ${it} -Format htm -o /tmp/"${data}nikto.html" """  
      sh"""cp /tmp/"${data}.html" ."""
      }
      html_report()
      notification.sendSlackNotification(
      slackChannel:   "${config.SLACK}",
      buildStatus:    "good",
      environment:    "${config.ENVIRONMENT}",
      message:        "Successfull in Performing NIKTO Testing and Publishing report"
      ) 
      
    }
  catch(e){
      notification.errorInfo(
      message: "Error in reading property file",
      slack_channel: "$config.SLACK"
      color: "danger"
      environment: "${config.ENVIRONMENT}"
      error: e
      )
    }    
  }


def html_report()
    {
    publishHTML([allowMissing: false, 
    alwaysLinkToLastBuild: false,
    keepAll: false, reportDir: '',
    reportFiles: '*.html',
    reportName: 'HTML Report',
    reportTitles: ''])
    }